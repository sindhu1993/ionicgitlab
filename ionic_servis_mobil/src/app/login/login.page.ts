import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  constructor(private route:Router) {}

  ngOnInit() {}

  password_type: string = 'password';
  username: string = '';
  password: string = '';
  showLoading : boolean = false;

  showPassword(e) {
    var statement = true;
    if (statement) {
      e.checked = true;
      if (this.password_type == 'password') {
        this.password_type = 'text';
      } else {
        this.password_type = 'password';
      }
    }
  }

  processLogin() {
    if (this.username == '' || this.password == '') {
      Swal.fire({
        title: 'Warning...',
        text: 'Please Input Username or Password',
        icon: 'warning',
      }).then((result)=>{
        if(result){
          this.showLoading = !this.showLoading;
        }
      });
    }
    else{
      // perform Http post to server
      // perform Http post to server

      Swal.fire({
        title: 'Success',
        text: 'Login Success, Klik Ok to Proceed',
        icon: 'success',
      }).then((result)=>{
        if(result){
          // this.route.navigate(['/home']);
          this.showLoading =!this.showLoading;
        }
      });
    }
  }
}
